// Imports
require("dotenv").config();
import Logs from "./util/logs";

const logs = Logs.get("glix");
Logs.get("domain");
Logs.get("auto");
Logs.get("db");
Logs.get("user");
Logs.get("mailer");
Logs.get("cache");
Logs.get("caddy");

import { connect } from "./db/connect";
import Users from "./stores/UserStore";
import Domains from "./stores/DomainStore";
import { connect as connectMail } from "./util/mail";
import { promisify } from "util";
import { App } from "./server/App";
import { createServer } from "http";
import { set } from "./util/proxy";

const delay = promisify(setTimeout);

(async () => {
	await connectMail();
	await connect();
	Domains; Users;
	
	const server = createServer(App);
	server.listen(80, "0.0.0.0", async () => {
		let localIP = server.address()! as any;
		let _ = "Unknown";
		if (localIP.family === "IPv4") _ = localIP.address + ":" + localIP.port;
		else if (localIP.family === "IPv6") _ = "[" + localIP.address + "]:" + localIP.port;
		
		logs.success("Server listening on " + _);
		await set(process.env.CADDY_DOMAIN!, "glix", 80);
	});
	
})().catch(error => logs.error(error.stack));
