// Imports
import logs from "../util/log/db";
import {
	connect as mConnect
} from "mongoose";

/**
 * Make a default connection to the MongoDB database.
 * 
 * **Note** that this required the MongoDB connection URI to be set
 * in the environment variables as `MONGODB_URI`.
 */
export const connect = async () => {
	
	logs.debug("Connecting to database!");
	
	const connection = await mConnect(
		process.env.MONGODB_URI!,
		{
			useCreateIndex: true,
			useFindAndModify: true,
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);
	
	logs.success("Connected to database!");
	
	return connection;
	
};
