// Imports
import * as Joi from "joi";
import {
	model,
	Schema,
	Document
} from "mongoose";
import Collection from "@discordjs/collection";
import { Entity } from "../Entity";
import { DomainStore } from "../../stores/DomainStore";
import { PendingUser } from "../../structure/PendingUser";
import { Pointer } from "../../structure/Pointer";
import { User } from "./User";
import Users from "../../stores/UserStore";
import { parse } from "psl";

/** Domain schema to use in the database. */
export const domainSchema = new Schema({
	host: { type: String, required: true, unique: true },
	verifiedAt: { type: Number, required: false, default: () => null },
	owner: { type: String, required: false, default: () => null },
	pending: [{
		user: { type: String, required: true },
		at: { type: Number, required: true }
	}],
	disabled: { type: Boolean, required: false, default: () => false },
	links: [{
		host: { type: String, required: true, unique: false },
		dial: { type: String, required: true, unique: false },
		port: { type: Number, required: true, unique: false }
	}]
});

/** The domain mongoose model. */
export const domainModel = model("Domain", domainSchema);

/** The domain creation schema validation object. */
export const domainCreateSchema = Joi.object().keys({
	host: Joi.string().domain().required(),
	pendingUser: Joi.string().required()
}).required();

/**
 * A domain entity.
 */
export class Domain extends Entity<DomainStore>
{
	
	/**
	 * Parse a hostname using PSL (Public Suffix List) module.
	 * @param host The hostname to parse.
	 */
	public static parse (host: string): ParseReturnType {
		const data = parse(host) as any;
		if (data.error) return {
			message: data.error.message,
			code: data.error.code,
			input: data.input,
			error: true
		};
		return {
			input: data.input,
			tld: data.tld,
			sld: data.sld,
			domain: data.domain,
			subdomain: data.subdomain,
			listed: data.listed,
			error: false
		};
	}
	
	/**
	 * Check whether or not a domain is valid.
	 * @param host The hostname to verify.
	 */
	public static verifyDomain (host: string): boolean
	{
		const parsed = Domain.parse(host);
		if (parsed.error) return false;
		if (parsed.input !== parsed.domain) return false;
		return true;
	}
	
	public static verifyHost (domain: string, host: string): boolean
	{
		const parsed = Domain.parse(host);
		if (parsed.error) return false;
		if (domain !== parsed.domain) return false;
		if (parsed.input !== host) return false;
		return true;
	}
	
	#doc: Document & DomainDocData;
	
	#pending: PendingUserData[];
	#pointers: PointerData[];
	#owner?: string;
	
	/** The domain name host. */
	public readonly host: string;
	
	/** A collection of pending users. */
	public readonly pending: Collection<string, PendingUser> = new Collection();
	
	/** A collection of pointers. */
	public readonly pointers: Collection<string, Pointer> = new Collection();
	
	/** The date the domain was verified. Defaults to null if the domain isn't verified. */
	public verifiedAt: Date | null;
	
	/** Whether or not this domain is disabled. */
	public disabled: boolean;
	
	/** The owner of this domain. Defaults to null if an owner is not set. */
	public owner?: User | null;
	
	/**
	 * Initiate a new domain entity. Do not create these yourselves.
	 * @param store The store that the entity belongs to.
	 * @param doc The document that the entity should wrap.
	 */
	public constructor (store: DomainStore, doc: Document & DomainDocData)
	{
		super(store, doc);
		this.#doc = doc;
		
		this.host = doc.host;
		this.disabled = doc.disabled;
		this.verifiedAt = typeof doc.verifiedAt === "number" ? new Date(doc.verifiedAt) : null;
		this.#pending = doc.pending;
		this.#pointers = doc.links;
		this.#owner = typeof doc.owner !== null && doc.owner ? (doc as Document & DomainDocData).owner!.toString() : undefined;
	}
	
	/**
	 * Load the owner or pending users of this 
	 */
	public async load (): Promise<this>
	{
		if (this.#owner)
		{
			this.owner = await Users.fetch(this.#owner);
			this.owner!.domains.set(this._id, this);
			return this;
		}
		this.pending.clear();
		for (let pending of this.#pending)
		{
			const pu = new PendingUser(this._id, pending as any);
			this.pending.set(pending.user.toString(), pu);
			const user = await pu.user()!;
			user!.pending.set(this._id, pu);
		}
		for (let pointer of this.#pointers)
		{
			const p = new Pointer(this, pointer);
			this.pointers.set(pointer.host, p);
			await p.ensure();
		}
		return this;
	}
	
	/**
	 * Set the owning user of the domain.
	 * @param user The user object.
	 */
	public async owned (user: User): Promise<this>
	{
		this.pending.clear();
		this.owner = user;
		this.verifiedAt = new Date();
		await this.save();
		return this;
	}
	
	/**
	 * Set the changes 
	 */
	public async save (): Promise<this>
	{
		(this.#doc.pending as any) = this.pending.array().map(pendingUser => pendingUser.data());
		(this.#doc.links as any) = this.pointers.array().map(pointer => pointer.data());
		this.#doc.owner = this.owner ? this.owner._id : null;
		this.#doc.disabled = this.disabled;
		this.#doc.verifiedAt = this.verifiedAt ? this.verifiedAt.getTime() : null;
		return await super.save();
	}
	
	/**
	 * Add a pending user.
	 * @param user The pending user.
	 */
	public async add (user: User): Promise<string>
	{
		if (!user.verifiedAt) throw new Error("User must be verified!");
		(this.#doc.pending as any) = this.pending.array().map(pendingUser => pendingUser.data());
		this.#doc.pending.push({
			user: user._id,
			at: Date.now()
		});
		this.#pending = this.#doc.pending;
		await this.#doc.save();
		await this.load();
		await this.save();
		return await user.sendPendingDomainEmail(this, this.pending.get(user._id)!);
	}
	
	/**
	 * Create a new pointer.
	 * @param host The host to receive requests from.
	 * @param dial The target host to forward requests to.
	 * @param port The port to connect to.
	 */
	public async set (host: string, dial: string, port: number)
	{
		host = host.toLowerCase();
		dial = dial.toLowerCase();
		if (!Domain.verifyHost(this.host, host)) throw new Error("Invalid host!");
		if (this.pointers.has(host))
		{
			await this.pointers.get(host)!.delete();
		}
		const pointer = new Pointer(this, { host, dial, port });
		this.pointers.set(pointer.host, pointer);
		await this.save();
		await pointer.ensure();
	}
	
	/**
	 * Unset a host from the proxy.
	 * @param host The host.
	 */
	public async unset (host: string)
	{
		host = host.toLowerCase();
		if (!this.pointers.has(host)) return;
		await this.pointers.get(host)!.delete();
	}
	
}

export type DomainDocData = {
	host: string,
	verifiedAt: null | number,
	owner: null | string,
	disabled: boolean,
	pending: PendingUserData[],
	links: PointerData[],
};

export type PointerData = {
	host: string,
	dial: string,
	port: number
};

export type PendingUserData = {
	user: string,
	at: number
};

export type ParseReturnType = {
	error: false,
	tld: null | string,
	sld: null | string,
	domain: null | string,
	subdomain: null | string,
	listed: boolean,
	input: string
} | {
	error: true,
	code: string,
	message: string,
	input: string
};
