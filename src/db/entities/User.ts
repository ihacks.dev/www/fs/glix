// Imports
import * as Joi from "joi";
import {
	model,
	Schema,
	Document
} from "mongoose";
import { Entity } from "../Entity";
import { UserStore } from "../../stores/UserStore";
import { hash, verify } from "../../util/argon2";
import { connect } from "../../util/mail";
import {
	sign,
	verify as verifyJWT
} from "jsonwebtoken";
import logs from "../../util/log/user";
import { createHmac } from "crypto";
import { Domain } from "./Domain";
import { PendingUser } from "../../structure/PendingUser";
import Collection from "@discordjs/collection";
import Domains from "../../stores/DomainStore";

const importTime: string = Date.now().toString();

const sixNine = BigInt(999999);

/**
 * The user schema used in the MongoDB database.
 */
export const userSchema = new Schema({
	displayName: { type: String, required: true },
	email: { type: String, required: true, unique: true },
	hash: { type: String, required: true },
	
	admin: { type: Boolean, required: false, default: () => false },
	beta: { type: Boolean, required: false, default: () => false },
	
	verifiedAt: { type: Number, required: false, default: () => null },
	
	tv: { type: Number, required: false, default: () => 0 }
});

/** The Mongoose user model. */
export const userModel = model("User", userSchema);

/** The user validation schema for user creation. */
export const userCreateSchema = Joi.object().keys({
	email: Joi.string().email().required(),
	displayName: Joi.string().min(2).max(32).lowercase().regex(/^[a-zA-Z0-9_\-]+$/),
	password: Joi.string().min(8)
}).required();

/**
 * An user entity object.
 */
export class User extends Entity<UserStore>
{
	
	#doc: Document & UserDocData;
	
	/** A fixed displayName. */
	public readonly name: string;
	
	/** The display name of the user. */
	public readonly displayName: string;
	
	/** The user's email address. */
	public readonly email: string;
	
	/**
	 * The date *this* user was verified. Defaults to `null` if the
	 * user isn't verified yet.
	 */
	public verifiedAt: Date | null;
	
	/** Whether or not this account was created during the beta period. */
	public beta: boolean;
	
	/** Whether or not this account has full priveleges on the system. */
	public admin: boolean;
	
	/** The user's password hash. */
	public hash: string;
	
	/** The user's ID but in an ObjectId. */
	public __id: Schema.Types.ObjectId;
	
	public readonly domains: Collection<string, Domain> = new Collection();
	public readonly pending: Collection<string, PendingUser> = new Collection();
	
	public readonly verificationKey: string;
	
	/**
	 * Initiate a new user entity. Do not create these yourselves.
	 * @param store The store that the entity belongs to.
	 * @param doc The document that the entity should wrap.
	 */
	public constructor (store: UserStore, doc: Document & UserDocData)
	{
		super(store, doc);
		this.#doc = doc;
		this.__id = doc._id;
		
		// Set properties.
		this.displayName = doc.displayName;
		this.verifiedAt = doc.verifiedAt ? new Date(doc.verifiedAt) : null;
		this.email = doc.email;
		this.beta = doc.beta;
		this.admin = doc.admin;
		this.hash = doc.hash;
		
		this.name = this.displayName.substring(0, 1).toUpperCase() + this.displayName.substring(1, this.displayName.length).toLowerCase();
		
		this.verificationKey = Number(BigInt("0x" + this._id) % sixNine).toString().padStart(6, "0");
	}
	
	/**
	 * Load the given user.
	 */
	public async load (): Promise<this>
	{
		let domains = await Domains.by(this);
		for (let domain of domains)
		{
			this.domains.set(domain._id, domain);
		}
		return this;
	}
	
	/**
	 * Set the changed properties on the document and save.
	 */
	public async save (): Promise<this>
	{
		if (this.#doc.verifiedAt === null && this.verifiedAt !== null) this.#doc.verifiedAt = this.verifiedAt.getTime();
		if (this.verifiedAt instanceof Date && this.#doc.verifiedAt !== this.verifiedAt.getTime()) this.#doc.verifiedAt = this.verifiedAt.getTime();
		if (this.#doc.beta !== this.beta) this.#doc.beta = this.beta;
		if (this.#doc.admin !== this.admin) this.#doc.admin = this.admin;
		if (this.#doc.hash !== this.hash) this.#doc.hash = this.hash;
		logs.debug("Updating user '%s' (%s)", this._id, this.email);
		await super.save();
		logs.info("Updated user %s", this.email);
		return this;
	}
	
	/**
	 * Test some password against the user's password hash.
	 * @param password The raw password to test.
	 */
	public async test (password: string): Promise<boolean>
	{
		return await verify(this.hash, password);
	}
	
	/**
	 * Change the user's password hash with a new password.
	 * @param newPassword The new password.
	 */
	public async changePassword (newPassword: string): Promise<this>
	{
		logs.debug("Updating user '%s' (%s): hash", this._id, this.email);
		this.hash = await hash(newPassword);
		await this.save();
		logs.success("Updated user '%s' (%s): hash", this._id, this.email);
		return this;
	}
	
	/**
	 * Delete the user from the database.
	 * 
	 * **Note** that this does not delete the user from the user
	 * store.
	 */
	public async delete (): Promise<void>
	{
		logs.debug("Deleting user '%s' (%s)", this._id, this.email);
		for (let [, domain] of this.domains) await domain.delete();
		for (let [, pu] of this.pending) await (await pu.domain())?.delete()
		await super.delete();
		logs.success("Deleted user '%s' (%s)", this._id, this.email);
	}
	
	public async fetchDomains (): Promise<void>
	{
		await Domains.fetch(this._id, true);
	}
	
	/**
	 * Send the user an email.
	 * @param subject The subject of the email.
	 * @param html The HTML content of the email.
	 */
	public async send (subject: string, html: string)
	{
		logs.debug("Sending mail to user '%s', subject '%s' with custom html.", this.email, subject);
		return await (await connect()).send(this.email, subject, html);
	}
	
	/**
	 * Send the user an email using an EJS template.
	 * @param subject The subject of the email.
	 * @param template The template name.
	 * @param data The data to pass to the template compiler.
	 */
	public async sendTemplate (subject: string, template: string, data: any)
	{
		logs.debug("Sending mail to user '%s', subject '%s' with template '%s'.", this.email, subject, template);
		return await (await connect()).sendTemplate(this.email, subject, template, data);
	}
	
	/**
	 * Create a user verification token.
	 */
	public createVerificationToken (): string
	{
		return sign({
			email: this.email
		}, process.env.ARGON2_SECRET! + importTime, {
			expiresIn: "1d"
		});
	}
	
	/**
	 * Send the user a verification token by email.
	 */
	public async sendVerificationEmail ()
	{
		// const jwt = this.createVerificationToken();
		// const host = process.env.CADDY_DOMAIN!;
		return await this.sendTemplate(`Hello, ${this.name}! Please verify your email account!`, "users/verify", {
			name: this.name,
			code: this.verificationKey
		});
	}
	
	/**
	 * Send the user a verification email for a pending domain.
	 */
	public async sendPendingDomainEmail (domain: Domain, pending: PendingUser)
	{
		const token = await pending.createVerificationToken();
		await this.sendTemplate("Please verify your domain.", "domains/verify", {
			domain: domain.host,
			name: this.name,
			token
		});
		return "GLIX-VERIFY=" + token;
	}
	
	/**
	 * Verify the user using a verification token.
	 * @param token The verification token.
	 */
	public async verify (token: string): Promise<boolean>
	{
		try
		{
			logs.debug("Verifying verification token for user %s", this.email);
			const result = verifyJWT(token, process.env.ARGON2_SECRET! + importTime) as any;
			if (result.email !== this.email) throw new Error("Email mismatch!");
			this.verifiedAt = new Date();
			await this.save();
			return true;
		} catch (error)
		{
			logs.warn("User verification failed for %s", this.email);
			logs.error(error);
			return false;
		}
	}
	
	/**
	 * Hash the access token.
	 * @param token The access token.
	 */
	private _hash (token: string): string
	{
		return createHmac("sha512", process.env.ARGON2_SECRET! + token).update(process.env.ARGON2_SECRET! + importTime + token).digest("hex").toString();
	}
	
	/**
	 * Test if the access token is matching.
	 * @param token The access token.
	 * @param data The previously hashed access token.
	 */
	private testAccessToken (token: string, data: string): boolean
	{
		return data === this._hash(token);
	}
	
	public async expireTokens ()
	{
		this.#doc.tv = (this.#doc.tv + 1) % 4_294_967_295;
		await this.#doc.save();
	}
	
	/**
	 * Create a set of API tokens.
	 */
	public createTokens (): Tokens
	{
		
		const access = sign({
			id: this._id,
			v: this.#doc.tv
		}, process.env.ARGON2_SECRET! + importTime, {
			expiresIn: "1m"
		});
		
		// @FIXME: This is not a good idea in the long run,
		//         and must be changed when website is running.
		//         This refresh token never expires!
		const refresh = this._hash(access);
		
		return [ access, refresh ];
	}
	
	/**
	 * Verify that a given access token is valid.
	 * @param token The access token.
	 */
	public verifyAccessToken (token: string): boolean
	{
		try
		{
			const data = verifyJWT(token, process.env.ARGON2_SECRET! + importTime) as any;
			if (data.id !== this._id) throw new Error("ID mismatch!");
			if (data.v !== this.#doc.tv) throw new Error("Version mismatch!");
			return true
		} catch (error)
		{
			return false;
		}
	}
	
	/**
	 * Verify the refresh token.
	 * @param token The access token used to create the refresh token.
	 * @param refreshToken The refresh token.
	 */
	public verifyRefreshToken (token: string, refreshToken: string): boolean
	{
		return this.testAccessToken(token, refreshToken);
	}
	
}

export type UserCreate = {
	displayName: string;
	email: string;
	password: string;
};

export type UserDocData = {
	displayName: string,
	verifiedAt: number | null,
	email: string
	beta: boolean,
	admin: boolean,
	hash: string,
	tv: number
};


export type AccessToken = string;
export type RefreshToken = string;
export type Tokens = [ AccessToken, RefreshToken ];
