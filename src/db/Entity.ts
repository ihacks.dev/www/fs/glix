// Imports
import { Document } from "mongoose";
import { Store } from "../structure/Store";

/**
 * An entity is something that holds a Mongoose document,
 * but wraps is in an object and adds utiilty methods to it.
 */
export class Entity<S extends Store<any>>
{
	
	#doc: Document;
	#id: string;
	
	/** The store where the entity belongs. */
	public readonly store: S;
	
	/** The ObjectID of the document. */
	public readonly _id: string;
	
	/** The creation date of the document. */
	public readonly created_at: Date;
	
	/**
	 * Initiate a new entity.
	 * @param store The store that the entity belongs to.
	 * @param doc The entity document.
	 */
	public constructor (store: S, doc: Document)
	{
		this.#doc = doc;
		this.store = store;
		this.#id = doc._id.toString();
		this._id = doc._id.toString();
		this.created_at = new Date(doc._id.getTimestamp());
	}
	
	/**
	 * Touch the document so it lives longer in the store.
	 */
	public touch (): this
	{
		if (!this.store.has(this.#id)) this.store.set(this.#id, this);
		else this.store.touch(this.#id);
		return this;
	}
	
	/**
	 * Save changes on the document.
	 * 
	 * Remember to set the changes before saving!
	 */
	public async save (): Promise<this>
	{
		await this.touch().#doc.save();
		return this;
	}
	
	/**
	 * Delete the document from the database, **this does not delete
	 * it from the store.**
	 */
	public async delete ()
	{
		await this.#doc.remove();
	}
	
}
