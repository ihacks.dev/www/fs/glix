// Imports
import { Router } from "express";
export const router = Router();
export default router;

// API Endpoints
import User from "./user";

// Use APIs
router.use("/users", User);
