export class MiddlewareError extends Error
{
	public code: number;
	public json ()
	{
		return {
			code: this.code,
			message: this.message
		};
	}
	public constructor (code: number, message: string)
	{
		super(message);
		this.code = code;
	}
}

// Exports
export const e404 = new MiddlewareError(404, "Not found!");
export const unauth = new MiddlewareError(403, "Unexpected authorization header!");
export const insecure = new MiddlewareError(426, "Request can only be handled over a secure connection!");
export const missingAuthHeader = new MiddlewareError(401, "Missing authentication header!");
export const invalidAuthHeader = new MiddlewareError(401, "Authentication token is invalid or expired!");
export const unverifiedUser = new MiddlewareError(401, "User must be verified to perform this action!");
export const invalidVerificationCode = new MiddlewareError(418, "The user verification code was invalid!");

// API Requests General.
export const invalidBody = new MiddlewareError(400, "Missing or invalid body in request!");

// User Errors
export const missingDisplayName = new MiddlewareError(400, "Missing display name in request body.");
export const missingEmail = new MiddlewareError(400, "Missing email in request body!");
export const missingPassword = new MiddlewareError(400, "Missing password in request body!");

// User creation.
export const shortDisplayName = new MiddlewareError(400, "The display name is too short, use at least 2 characters.");
export const longDisplayName = new MiddlewareError(400, "The display name is too long, use at maximum 32 characters.");
export const invalidDisplayName = new MiddlewareError(400, "The display name contains invalid characters, please only use characters 'A-Z', 'a-z', '0-9', '_' and '-' !");

export const invalidEmail = new MiddlewareError(400, "The email is invalid!");
export const emailTaken = new MiddlewareError(400, "The email is already taken!");

export const shortPassword = new MiddlewareError(400, "Password is too short, use at minimum 8 characters.");
export const invalidPassword = new MiddlewareError(400, "Password must at least contain 1 lowercase letter, 1 uppercase letter, 1 number and 1 symbol.");

// User login.
export const invalidLogin = new MiddlewareError(418, "Invalid email or password!");
