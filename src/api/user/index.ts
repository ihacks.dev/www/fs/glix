// Meta Import
import Users from "../../stores/UserStore";

// Imports
import { Router } from "express";
import * as Joi from "joi";
import {
	f, a,
	expectUnAuth,
	expectAuth,
	allowUnverified
} from "../middleware";
import {
	invalidBody,
	missingDisplayName,
	shortDisplayName,
	longDisplayName,
	invalidDisplayName,
	missingEmail,
	invalidEmail,
	missingPassword,
	shortPassword,
	invalidPassword,
	emailTaken,
	invalidLogin,
	invalidVerificationCode
} from "../errors";

// The user router.
export const router = Router();
export default router;

// Other

const _email = Joi.string().email().required();
const isEmail = (s: string): boolean => {
	const res = _email.validate(s);
	return !res.error && !res.errors;
};
const containsLowerCase = (s: string): boolean => /[a-zæøå]/g.test(s);
const containsUpperCase = (s: string): boolean => /[A-ZÆØÅ]/g.test(s);
const containsNumber = (s: string): boolean => /[0-9]/g.test(s);
const containsSymbol = (s: string): boolean => /[^a-zA-Z0-9æøåÆØÅ]/g.test(s);
const containsAll = (s: string): boolean => true
	&& containsLowerCase(s)
	&& containsUpperCase(s)
	&& containsNumber(s)
	&& containsSymbol(s);

// Use APIs

router.post(
	"/create",
	expectUnAuth,
	f(async req => {
		if (!req.body || typeof req.body !== "object" || req.body === null) throw invalidBody;
		if (!req.body.displayName || typeof req.body.displayName !== "string") throw missingDisplayName;
		if (req.body.displayName.length < 2) throw shortDisplayName;
		if (req.body.displayName.length > 32) throw longDisplayName;
		if (!/^([a-z0-9\_\-]+)$/gi.test(req.body.displayName)) throw invalidDisplayName;
		if (!req.body.email || typeof req.body.email !== "string") throw missingEmail;
		if (!isEmail(req.body.email)) throw invalidEmail;
		if (!req.body.password || typeof req.body.password !== "string") throw missingPassword;
		if (req.body.password.length < 8) throw shortPassword;
		if (!containsAll(req.body.password)) throw invalidPassword;
		const emailHolder = await Users.fetch(req.body.email);
		if (emailHolder) throw emailTaken;
		const user = await Users.create({
			email: req.body.email,
			displayName: req.body.displayName,
			password: req.body.password
		});
		await user.sendVerificationEmail();
		return user._id;
	})
);

router.post(
	"/login",
	expectUnAuth,
	f(async (req, res) => {
		if (!req.body || typeof req.body !== "object" || req.body === null) throw invalidBody;
		if (!req.body.email || typeof req.body.email !== "string") throw missingEmail;
		if (!req.body.password || typeof req.body.password !== "string") throw missingPassword;
		const user = await Users.fetch(req.body.email);
		if (!user) throw invalidLogin;
		const passwordMatch = await user.test(req.body.password);
		if (!passwordMatch) throw invalidLogin;
		const [ token, refreshToken ] = user.createTokens();
		res.header("x-refresh-token", refreshToken);
		res.header("x-new-token", token);
		res.header("x-new-refresh-token", refreshToken);
		return [ token, refreshToken, user.verifiedAt === null ? true : undefined ];
	})
);

router.get(
	"/me/:code?",
	allowUnverified,
	expectAuth,
	a(async req => {
		
		if (req.params.code)
		{
			if (req.params.code === req.user.verificationKey)
			{
				req.user.verifiedAt = new Date();
				await req.user.save();
			} else
			{
				throw invalidVerificationCode;
			}
		}
		
		return {
			id: req.user._id,
			displayName: req.user.displayName,
			email: req.user.email,
			admin: req.user.admin,
			beta: req.user.beta,
			verified: req.user.verifiedAt !== null
		};
	})
);
