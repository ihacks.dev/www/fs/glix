// Imports
import {
	NextFunction,
	Request,
	Response
} from "express";
import { decode } from "jsonwebtoken";
import {
	e404,
	MiddlewareError,
	unauth,
	missingAuthHeader,
	invalidAuthHeader,
	unverifiedUser
} from "../errors";
import logs from "../../util/log/glix";
import { User } from "../../db/entities/User";
import Users from "../../stores/UserStore";

type CR = Request & { meta: { [key: string]: any } };

type mwr = (req: CR, res: Response) => Promise<any>;
type mwrAuthenticated = (req: CR & { user: User }, res: Response) => Promise<any>;
type mw = (req: Request, res: Response, next: NextFunction) => Promise<any>;

export const mw = (fn: mwr, callNext: boolean = true): mw => async (req, res, next) => {
	const r = req as CR;
	if (!r.meta) r.meta = {};
	let e: {e:Error} = {} as any;
	if (!res.hasHeader("x-request-received"))
		res.setHeader("x-request-received", Date.now());
	fn(r, res)
		.then(data => callNext ? next() : data !== undefined ? res.status(200).json(data).end() : null)
		.catch(error => {
			e.e = error;
		})
		.finally(() => {
			if (e.e instanceof MiddlewareError) res.status(e.e.code).json(e.e.json());
			else if (e.e instanceof Error) res.status(500).json({ code: 500, message: "Server error" });
			const ms = String(Date.now() - Number(res.getHeader("x-request-received")!)).padStart(5, " ") + " ms";
			const ip = req.connection.remoteAddress!.split(".").map(p => p.padStart(3, "0")).join(".");
			const p = req.originalUrl;
			const s: any[] = ["{%s} %s - %d %s %s", ip, ms, res.statusCode, req.method.padEnd(6), p];
			if (!e.e && !callNext) logs.debug(...s);
			else if (e.e)
			{
				if (!!e.e)
				{
					s[0] += " | %s";
					s.push(e.e instanceof MiddlewareError ? e.e.message : e.e.stack);
				}
				logs.error(...s);
			}
		});
};

export const f = (fn: mwr): mw => mw(fn, false);
export const a = (fn: mwrAuthenticated): mw => mw(fn as any, false);
export const m = (fn: mwr): mw => mw(fn, true);

export const error404 = m(async () => {
	throw e404;
})

export const expectUnAuth = m(async req => {
	if (req.headers.authorization) throw unauth;
});

export const allowUnverified = m(async req => req.meta.allowUnverified = true);

export const expectAuth = m(async (req, res) => {
	if (!req.headers.authorization) throw missingAuthHeader;
	const token = req.headers.authorization!;
	const data = decode(token) as any;
	if (!data || !data.id || typeof data.id !== "string") throw invalidAuthHeader;
	let user = await Users.fetch(data.id);
	if (!user) throw invalidAuthHeader;
	if (!req.meta.allowUnverified && user.verifiedAt === null) throw unverifiedUser;
	if (!user.verifyAccessToken(token) || req.headers["x-refresh-token"] !== undefined)
	{
		const refreshToken = req.headers["x-refresh-token"] as string;
		if (refreshToken && user.verifyRefreshToken(token, refreshToken))
		{
			const [ token, refreshToken ] = user.createTokens();
			res.setHeader("x-new-token", token);
			res.setHeader("x-new-refresh-token", refreshToken);
			(req as any).user = user;
			return;
		}
		throw invalidAuthHeader;
	}
	(req as any).user = user;
	return;
});
