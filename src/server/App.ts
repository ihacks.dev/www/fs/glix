// Imports
import * as Express from "express";
import API from "../api";
import { error404 } from "../api/middleware";

/**
 * The express application.
 */
export const App = require("express")() as Express.Application;

// Trust all proxies, hence only the glix proxy will come through.
App.set("trust proxy", true);

// Middleware.
App.use(Express.json());
App.use(Express.urlencoded({ extended: true }));
App.use(API);
App.use(error404);
