// Imports
import { User } from "../db/entities/User";
import Users from "../stores/UserStore";
import Domains from "../stores/DomainStore";
import { Domain } from "../db/entities/Domain";
import { Schema } from "mongoose";
import {
	sign,
	verify as verifyJWT
} from "jsonwebtoken";
import logs from "../util/log/domain";

/**
 * A structure that belongs to the domain category.
 * 
 * A pending user is a user of which is waiting to get their domain verified.
 */
export class PendingUser
{
	
	#userID: Schema.Types.ObjectId;
	#user: string;
	
	
	#domain: string;
	
	/** The date this user was added to the pending users list. */
	public readonly at: Date;
	
	/**
	 * Initiate a pending user.
	 * @param domain The domain object that the pending user is from.
	 * @param pendingUser The pending user object.
	 */
	public constructor (domain: string, pendingUser: PendingUserData)
	{
		this.at = new Date(pendingUser.at);
		this.#userID = pendingUser.user;
		this.#user = this.#userID.toString();
		this.#domain = domain;
	}
	
	/**
	 * Fetch the domain that this pending user belongs to.
	 */
	public async domain (): Promise<Domain | undefined>
	{
		return await Domains.fetch(this.#domain);
	}
	
	/**
	 * Fetch the user.
	 */
	public async user (): Promise<User | undefined>
	{
		const user = await Users.fetch(this.#user);
		if (!user) await this.delete();
		return user;
	}
	
	/**
	 * Delete the pending user from the domain.
	 */
	public async delete ()
	{
		const domain = await this.domain();
		if (!domain) throw new Error("Domain no longer exists!");
		domain.pending.delete(this.#user);
		await domain.save();
	}
	
	/**
	 * Make this user the owner of the pending domain.
	 */
	public async own (): Promise<Domain>
	{
		const domain = await this.domain();
		if (!domain) throw new Error("Domain no longer exists!");
		const user = await this.user();
		if (user) await domain.owned(user);
		return domain;
	}
	
	/**
	 * Turn the data into data that can be used in mongoose.
	 */
	public data (): PendingUserJSON
	{
		return {
			at: this.at.getTime(),
			user: this.#userID
		}
	}
	
	/**
	 * Create a verification token.
	 */
	public async createVerificationToken (): Promise<string>
	{
		const domain = <NonNullable<Domain>> await this.domain()!;
		const user = <NonNullable<User>> await this.user()!;
		return sign({
			host: domain.host,
			user: user._id
		}, process.env.ARGON2_SECRET!, {
			expiresIn: "999d"
		});
	}
	
	/**
	 * Verify the pending user using a verification token.
	 * @param token The verification token.
	 */
	public async verify (token: string): Promise<boolean>
	{
		const domain = <NonNullable<Domain>> await this.domain()!;
		const user = <NonNullable<User>> await this.user()!;
		if (!domain || !user) return false;
		try
		{
			logs.debug("Verifying verification token for user %s on domain %s.", user.email, domain.host);
			const result = verifyJWT(token, process.env.ARGON2_SECRET!) as any;
			if (result.host !== domain.host) throw new Error("Host mismatch!");
			if (result.user !== user._id) throw new Error("UserID mismatch!");
			domain.verifiedAt = new Date();
			await this.own();
			return true;
		} catch (error)
		{
			logs.warn("Pending user verification failed for user %s on domain %s", user.email, domain.host);
			logs.error(error);
			return false;
		}
	}
	
}

export type PendingUserData = {
	user: Schema.Types.ObjectId,
	at: number
};

export type PendingUserJSON = {
	user: Schema.Types.ObjectId,
	at: number
};
