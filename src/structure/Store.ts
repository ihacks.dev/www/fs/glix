// Imports
import {
	Cache,
	Entity
} from "./Cache";

/**
 * A store to store information such as users or domains.
 */
export class Store<T extends Entity> extends Cache<T>
{
	
}
