// Imports
import Collection from "@discordjs/collection";
import logs from "../util/log/cache";

/**
 * A cache utility tool.
 */
export class Cache<E extends Entity> extends Collection<string, E>
{
	
	#_: Collection<string, Entry> = new Collection();
	#age: number;
	#rate: number;
	#interval: number | undefined;
	#name: string;
	
	/**
	 * Initiate a new cache collection.
	 * @param name The name of the cache.
	 * @param maxAge The max age each entry can live unless touched.
	 * @param rate The ms interval the reducer should run at.
	 */
	public constructor (name: string, maxAge: number = 60_000, rate: number = 60_000)
	{
		super();
		this.#name = name;
		this.#age = maxAge;
		this.#rate = rate;
	}
	
	/**
	 * Start the cache reducer.
	 */
	public start (): this
	{
		if (!this.#interval)
		{
			this.#interval = setInterval(() => {
				const now = Date.now();
				let a: number = 0;
				for (let [ k, entry ] of this.#_)
				{
					if (now > entry.exp)
					{
						this.delete(entry.id);
						this.#_.delete(entry.id);
						a++;
					}
				}
				if (a > 0) logs.debug("Cache %s lost %s entries.", this.#name, a);
			}, this.#rate) as any as number;
			logs.debug("Cache %s started purge interval.", this.#name);
		}
		return this;
	}
	
	/**
	 * Stop the cache reducer.
	 */
	public stop (): this
	{
		if (this.#interval)
		{
			clearInterval(this.#interval as any);
			this.#interval = undefined;
			logs.debug("Cache %s stopped purge interval.", this.#name);
		}
		return this;
	}
	
	private rawget (id: string): E | undefined
	{
		return super.get(id);
	}
	
	/**
	 * Touch an item and reset it's lifetime.
	 */
	public touch (id: string): this
	{
		if (this.has(id))
		{
			logs.debug("Cache %s touched %s", this.#name, id);
			this.#_.set(id, { exp: Date.now() + this.#age, id });
		}
		return this;
	}
	
	/**
	 * Fetch and touch an entry.
	 */
	public get (id: string): E | undefined
	{
		return this.touch(id).rawget(id);
	}
	
	/**
	 * Set an entry.
	 */
	public set (id: string, value: E): this
	{
		this.#_.set(id, { exp: Date.now() + this.#age, id });
		super.set(id, value);
		return this;
	}
	
	/**
	 * Add entities to the collection.
	 * @param entities The entities.
	 */
	public add (...entities: E[]): this
	{
		for (let entity of entities) this.set(entity._id, entity);
		return this;
	}
	
	/**
	 * Remove entities from the collection.
	 * @param entities The entities.
	 */
	public remove (...entities: E[]): this
	{
		for (let entity of entities) this.delete(entity._id);
		return this;
	}
	
}

export type Entity =
{
	_id: string;
}

export type Entry =
{
	exp: number;
	id: string;
}
