// Imports
import { Domain } from "../db/entities/Domain";
import Domains from "../stores/DomainStore";
import {
	has,
	remove,
	set
} from "../util/proxy";

/**
 * A pointer that points a host to a different host.
 */
export class Pointer
{
	
	#domain: string;
	
	/** The host that this pointer belongs to. */
	public readonly host: string;
	
	/** The target host, this is where requests will be forwarded to. */
	public readonly dial: string;
	
	/** The port to connect to. */
	public readonly port: number;
	
	/**
	 * Initiate the pointer.
	 * @param domain The domain that owns this pointer.
	 * @param data The data object.
	 * @param data.host The host.
	 * @param data.dial The target host.
	 * @param data.port The port.
	 */
	public constructor (domain: Domain, { host, dial, port }: PendingConstructorData)
	{
		this.#domain = domain._id;
		this.host = host;
		this.dial = dial;
		this.port = port;
	}
	
	/**
	 * Load the given pointer, and add it to the proxy if it isn't
	 * already there.
	 */
	public async ensure (): Promise<this>
	{
		await set(this.host, this.dial, this.port);
		return this;
	}
	
	/**
	 * Fetch the domain that this pointer belongs to.
	 */
	public async domain ()
	{
		return await Domains.fetch(this.#domain);
	}
	
	/**
	 * Delete the current pointer.
	 */
	public async delete ()
	{
		const domain = await this.domain();
		if (!domain) return;
		domain.pointers.delete(this.host);
		await remove(this.host);
		await domain.save();
	}
	
	public data (): PendingConstructorData
	{
		return {
			host: this.host,
			dial: this.dial,
			port: this.port
		};
	}
}

export type PendingConstructorData = {
	host: string,
	dial: string,
	port: number
};
