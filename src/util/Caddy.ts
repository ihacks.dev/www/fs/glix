// Imports
import fetch from "node-fetch";
import logs from "./log/caddy";

/**
 * Interact with the Caddy API.
 */
export class Caddy
{
	/**
	 * Execute something on the caddy API.
	 * @param url The Caddy api endpoint.
	 * @param method The HTTP method.
	 * @param data The data to send with the request.
	 * @throws An error if something goes wrong.
	 * @returns Nothing.
	 */
	public static async exec (url: string, method: "GET" | "PUT" | "POST" | "PATCH" | "DELETE" = "GET", data?: any): Promise<any>
	{
		logs.debug("Executing %s %s", method, url);
		const response = await fetch(url, {
			method,
			...(() => typeof data !== "undefined" && method !== "GET" ? { body: JSON.stringify(data) } : {})(),
			headers: {
				"Content-Type": "application/json"
			}
		});
		const text = await response.text();
		if (text.trim().length > 0)
		{
			let err;
			try
			{
				if (method === "GET")
				{
					const data = JSON.parse(text);
					if (data === null) return null;
					if (typeof data === "object" && !data.error || typeof data !== "object") return data;
					err =
						new Error(data.error);
				} else
				{
					err = new Error(JSON.parse(text).error);
				}
			} catch (error)
			{
				err = new Error(text);
			}
			
			if (err) throw err;
		}
	}
	
	/**
	 * Initiate a new Caddy API object.
	 * @param host The caddy admin host.
	 * @param port The caddy admin port.
	 */
	public constructor (public readonly host: string = "127.0.0.1", public readonly port: number = 2019){};
	
	/**
	 * Builds a relative URI string to the caddy host.
	 * @param path The path.
	 */
	public url (path: string): string
	{
		return `http://${this.host}:${this.port}${path}`;
	}
	
	/**
	 * Make a request to the caddy API.
	 * @param path On this path.
	 * @param method Using the given method.
	 * @param data With this data.
	 */
	public async exec (path: string, method: "GET" | "PUT" | "POST" | "PATCH" | "DELETE" = "GET", data?: any): Promise<void>
	{
		return await Caddy.exec(this.url(path), method, data);
	}
	
	public async hasInitiated (): Promise<boolean>
	{
		return await this.exec("/config/apps/http", "GET") !== null;
	}
	
	public async init (): Promise<void>
	{
		if (await this.hasInitiated()) return (logs.success("Caddy has been initiated!"), undefined);
		logs.debug("Initiating!");
		await this.exec("/config/apps/http", "PUT", {
			http_port: 80,
			https_port: 443,
			servers: {
				srv0: {
					"@id": "server",
					listen: [
						":443"
					],
					routes: []
				}
			}
		});
		logs.success("Caddy has been initiated!");
	}
	
	/**
	 * Check if a domain is stored in the caddy configurations.
	 * @param domain The domain name.
	 */
	public async has (domain: string): Promise<boolean>
	{
		try
		{
			const d = await this.exec("/id/" + domain);
			return true;
		} catch (error)
		{
			if (error.message === `unknown object ID '${domain}'`) return false;
			throw error;
		}
	}
	
	/**
	 * Delete a domain from the caddy configurations.
	 * @param domain The domain to delete.
	 */
	public async delete (domain: string): Promise<void>
	{
		logs.debug("Deleting host %s", domain);
		await this.exec("/id/" + domain, "DELETE");
		logs.info("Deleted host %s", domain);
	}
	
	/**
	 * Add a domain to the caddy configurations.
	 * @param domain The domain to add.
	 * @param dial The dial address.
	 * @param port The port to forward requests to.
	 */
	public async add (domain: string, dial: string, port: number): Promise<void>
	{
		if (await this.has(domain))
		{
			await this.exec("/id/" + domain + "/handle/0/routes/2/handle/0/upstreams/0/dial", "PUT", dial + ":" + port);
			logs.success("Updated host %s to %s", domain, dial);
			return;
		}
		logs.debug("Adding host %s to %s", domain, dial);
		await this.exec("/config/apps/http/servers/srv0/routes", "POST", {
			"@id": domain,
			"handle": [
				{
					"handler": "subroute",
					"routes": [
						{
							"handle": [
								{
									"handler": "headers",
									"response": {
										"replace": {
											"Location": [
												{
													"replace": "https://",
													"search_regexp": "http://"
												},
												{
													"replace": "wss://",
													"search_regexp": "ws://"
												}
											]
										},
										"set": {
											"Strict-Transport-Security": [
												"max-age=31536000;"
											],
											"X-Proxied-By": [
												"Glix"
											]
										}
									}
								}
							]
						},
						{
							"handle": [
								{
									"body": "OK",
									"close": true,
									"handler": "static_response",
									"status_code": 200
								}
							],
							"match": [
								{
									"path": [
										"/glix/healthcheck"
									]
								}
							]
						},
						{
							"handle": [
								{
									"handler": "reverse_proxy",
									"headers": {
										"request": {
											"set": {
												"Host": [
													dial
												]
											}
										}
									},
									"transport": {
										"protocol": "http",
										"tls": port === 443 ? {} : undefined
									},
									"upstreams": [
										{
											"dial": dial + ":" + port
										}
									]
								}
							]
						}
					]
				}
			],
			"match": [
				{
					"host": [
						domain
					]
				}
			],
			"terminal": true
		});
		logs.success("Added host %s to %s", domain, dial);
	}
	
}
