// Imports
import { Caddy } from "./Caddy";

/** The caddy object. */
const caddy = new Caddy("caddy", 2019);

/**
 * Check if the reverse-proxy has a host.
 * @param host The host.
 */
export const has = async (host: string): Promise<boolean> => await caddy.has(host);

/**
 * Delete a host from the proxy.
 * @param host The host.
 */
export const remove = async (host: string): Promise<boolean> =>
{
	return await has(host) ? (await caddy.delete(host), true) : false;
}

/**
 * Set a host on the reverse-proxy. If the host already exists, then delete it to replace it.
 * @param host The host.
 * @param dial The dial host.
 * @param port The port.
 */
export const set = async (host: string, dial: string, port: number): Promise<boolean> =>
{
	const a = await remove(host);
	await caddy.add(host, dial, port);
	return a;
}
