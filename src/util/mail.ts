// Import
import {
	Mailer,
	SMTPConnectionOptions
} from "./Mailer";

/** Environment variables. */
const e: any = process.env;

/** Whether or not a default connection is already being made. */
let connecting: boolean = false;

/** The default mailer wrapper object. */
let mailer!: Mailer;

/**
 * Make a default connection to an SMTP server using the environment variables.
 * @param overrides The overrides if some environment variables aren't already defined.
 */
export const connect = async (overrides?: Partial<SMTPConnectionOptions>) => {
	
	// The connection options.
	const host = e.SMTP_HOST || overrides?.host;
	const port = e.SMTP_PORT || overrides?.port;
	const ssl  = e.SMTP_SSL  || overrides?.ssl;
	const user = e.SMTP_USER || overrides?.user;
	const pass = e.SMTP_PASS || overrides?.pass;
	
	// Find missing connection options.
	if (!host) throw new Error("Missing host in connection.");
	if (!port) throw new Error("Missing port in connection.");
	if (!ssl) throw new Error("Missing ssl in connection.");
	if (!user) throw new Error("Missing user in connection.");
	if (!pass) throw new Error("Missing pass in connection.");
	
	// Check if a default connection already exists.
	// If the default connection is made, use it.
	if (mailer) return mailer;
	
	// Check if a default connection is currently being made.
	// If a connection is being made, throw an error.
	if (connecting) throw new Error("Already connecting!");
	
	// Set connecting to true, so a new connection cannot be attempted.
	connecting = true;
	
	// The connection object.
	let connection: Mailer;
	
	try
	{
		// Make a connection and set it to the connection variable.
		connection = await Mailer.connect({
			host: String(host),
			port: Number(port),
			ssl: Boolean(ssl),
			user: String(user),
			pass: String(pass)
		});
	} catch (error)
	{
		// If the connection fails set connecting to false so a new
		// attempt can be made.
		connecting = false;
		
		// Re-throw the error.
		throw error;
	}
	
	// Set connecting to false, because the connection has been made.
	connecting = false;
	
	// Set the default mailer object, so it can be returned if a new connection
	// is attempted.
	mailer = connection!;
	
	// Return the connection.
	return connection!;
}

export default mailer;
