// Imports
import { createTransport } from "nodemailer";
import logs from "./log/mailer";
import * as htmlToText from "html-to-text";
import TemplateStore from "../stores/TemplateStore";

/**
 * A Node-Mailer wrapper.
 */
export class Mailer
{
	
	/**
	 * A macro for connecting to an SMTP server.
	 * @param options The connection options.
	 */
	public static async connect (options: SMTPConnectionOptions): Promise<Mailer>
	{
		return await new Mailer(options).connect();
	}
	
	
	/** Whether or not the mailer is already connected to the SMTP server. */
	#connected: boolean = false;
	/** Whether or not the mailer is currently attempting to make a connection. */
	#connecting: boolean = false;
	/** The Node-Mailer transport object. */
	#transport!: Transport;
	/** The connection options which should be used to make a connection. */
	#options: SMTPConnectionOptions;
	
	/**
	 * Initiate a new SMTP Mailer object.
	 * @param options The connection options.
	 */
	public constructor (options: SMTPConnectionOptions)
	{
		this.#options = options;
	}
	
	/**
	 * Attempt to make the connection.
	 */
	public async connect (): Promise<this>
	{
		if (this.#connected) throw new Error("Already connected!");
		if (this.#connecting) throw new Error("Already making a connection!");
		this.#connecting = true;
		
		logs.debug("Connecting to '%s:%d' as '%s'", this.#options.host, this.#options.port, this.#options.user);
		
		this.#transport = createTransport({
			host: this.#options.host,
			port: this.#options.port,
			secure: this.#options.ssl,
			auth: {
				user: this.#options.user,
				pass: this.#options.pass
			}
		});
		await this.#transport.verify();
		this.#connected = true;
		this.#connecting = false;
		
		logs.success("Connected to '%s:%d' as '%s'", this.#options.host, this.#options.port, this.#options.user);
		
		return this;
	}
	
	/**
	 * Send an email with custom HTML.
	 * @param email The email to send the HTML to.
	 * @param subject The subject of the email.
	 * @param html The HTML code.
	 * @param debug Whether or not to send debug messages.
	 */
	public async send (email: string, subject: string, html: string, debug: boolean = true)
	{
		if (!this.#connected) throw new Error("Not connected to SMTP server.");
		if (debug) logs.debug("Sending mail to %s with subject '%s' with custom html.", email, subject);
		const result = await this.#transport.sendMail({
			sender: this.#options.user,
			from: this.#options.user,
			to: email,
			subject,
			html,
			text: htmlToText.fromString(html, {
				wordwrap: 70
			})
		});
		logs.info("Sent mail to %s with subject '%s'", email, subject);
		return result;
	}
	
	/**
	 * Compile an EJS template to HTML and send it as an email.
	 * @param email The email to send the compiled html code to.
	 * @param subject The subject of the email.
	 * @param template The template name.
	 * @param data The data to pass to the template compiler.
	 */
	public async sendTemplate (email: string, subject: string, template: string, data: any)
	{
		if (!this.#connected) throw new Error("Not connected to SMTP server.");
		logs.debug("Sending mail to %s with subject '%s' using template '%s'", email, subject, template);
		const fn = await TemplateStore.getTemplate(template);
		const html = await fn(data);
		return await this.send(email, subject, html, false);
	}
	
}

/**
 * The node-mailer transport object.
 */
type Transport = ReturnType<typeof createTransport>;

/**
 * The SMTP connection details.
 */
export type SMTPConnectionOptions = {
	host: string,
	port: number,
	ssl: boolean,
	user: string,
	pass: string
};
