// Imports
import {
	Logger,
	LogLevel,
	LogFileTransport,
	LogConsoleTransport,
	ILogData
} from "ihacks-log";
import {
	reset,
	bold,
	underline,
	dim,
	red,
	yellow,
	blue,
	gray,
	cyan,
	green,
	white
} from "chalk";
import {
	join
} from "path";
import {
	existsSync,
	mkdirSync
} from "fs";

// O is a shortcut / alias for getting the console output of something.
const o = Logger.getConsoleOutput;

// Change the original function to trim the string returned from the original function.
Logger.getConsoleOutput = (message: string[], colors: boolean): string => o(message, colors).trim();

/**
 * Combine ANSI colors or ANSI styles.
 * @param styles The styles to combine.
 */
const combineColors = (...styles: any[]) =>
	/**
	 * Apply the styles to a string.
	 * @param str The string.
	 */
	(str: string): string =>
		// Use a reducer and apply the styles to the string.
		styles.reduce(
			/**
			 * A reducer function to apply styles to a string.
			 * @param str The string (previous) value.
			 * @param style The style function.
			 */
			(str: string, style: any) => style(str),
			str
		);

/**
 * A function to apply styles if useStyles is set to true.
 * @param str The string to apply styles to.
 * @param useStyles Whether or not to use ANSI styles.
 * @param styles The styles to apply to the string.
 */
const cz = (str: string, useStyles: boolean, ...styles: any[]) =>
	useStyles ? combineColors(...styles)(str) : str;

/**
 * Turn a number into a string and pad it with zeroes on the start.
 * @param n The number to stringify and pas.
 * @param l The amount of characters the string should be.
 */
const pad = (n: number, l: number = 2) => n.toString().padStart(l, "0");

/**
 * Get the level color of a level number.
 * @param level The level number.
 * @returns An ANSI style for the given level number.
 */
const getLevelColor = (level: LogLevel) =>
	  level === LogLevel.ERROR
	? combineColors(bold, red)
	: level === LogLevel.WARN
	? yellow
	: level === LogLevel.ALERT
	? blue
	: level === LogLevel.SUCCESS
	? green
	: level === LogLevel.INFO
	? cyan
	: level === LogLevel.DEFAULT
	? white
	: level === LogLevel.DEBUG
	? gray
	: reset
	;

/**
 * Get the level name of a level number.
 * @param level The level number.
 * @returns The level name.
 */
export const getLevelName = (level: LogLevel) =>
	  level === LogLevel.ERROR
	? "ERROR"
	: level === LogLevel.WARN
	? "WARN"
	: level === LogLevel.ALERT
	? "ALERT"
	: level === LogLevel.SUCCESS
	? "SUCCESS"
	: level === LogLevel.INFO
	? "INFO"
	: level === LogLevel.DEFAULT
	? ""
	: level === LogLevel.DEBUG
	? "DEBUG"
	: "UNKNOWN"
	;

/**
 * A cache value to find the longest logger name.
 */
let longest: number = 0;

/**
 * The formatter used to style and format messages that goes to the
 * console, or the process' stdout.
 * @param name The name of the logger.
 * @param useStyles Whether or not to apply styles to messages.
 */
const consoleFormatter = (name: string, useStyles: boolean) => ({ level, message, timestamp } : ILogData) => [
	cz("[", useStyles, reset),
	cz(pad(timestamp.getDate()), useStyles, yellow),
	cz("/", useStyles, cyan),
	cz(pad(timestamp.getMonth() + 1), useStyles, yellow),
	cz("/", useStyles, cyan),
	cz(pad(timestamp.getFullYear(), 4), useStyles, yellow),
	cz(" ", useStyles, reset),
	cz(pad(timestamp.getHours()), useStyles, yellow),
	cz(":", useStyles, cyan),
	cz(pad(timestamp.getMinutes()), useStyles, yellow),
	cz(":", useStyles, cyan),
	cz(pad(timestamp.getSeconds()), useStyles, yellow),
	cz(".", useStyles, cyan),
	cz(pad(timestamp.getMilliseconds(), 3), useStyles, yellow),
	cz("] (", useStyles, reset),
	cz(name.padEnd(longest!, " "), useStyles, dim, underline),
	cz(") ", useStyles, reset),
	cz(getLevelName(level).padEnd(7, " "), useStyles, getLevelColor(level), underline),
	cz(" ", useStyles, reset),
	cz(message, useStyles, reset),
	" "
].join("");

/**
 * The formatter used to format messages that goes into files.
 * @param name The name of the logger.
 */
const fileFormatter = (name: string) => ({ timestamp, message, level }: ILogData): string => [
	"[", pad(timestamp.getDate()),
	"/", pad(timestamp.getMonth() + 1),
	"/", pad(timestamp.getFullYear(), 4),
	"-", pad(timestamp.getHours()),
	":", pad(timestamp.getMinutes()),
	":", pad(timestamp.getSeconds()),
	".", pad(timestamp.getMilliseconds(), 3),
	"] ", getLevelName(level).padEnd(7, " "),
	" ", JSON.stringify(name),
	" ", JSON.stringify(message),
	"\n"
].join("");

/**
 * A log store to keep track of created loggers.
 */
export class LogStore extends Map<string, Logger>
{
	public constructor (public logDir: string){super();}
	
	/**
	 * Get a or create a logger and store it.
	 * @param name The name of the logger.
	 */
	public get (name: string): Logger
	{
		if (!existsSync(this.logDir)) mkdirSync(this.logDir, { recursive: true });
		if (this.has(name)) return super.get(name)!;
		const logger = new Logger();
		
		const emitLevel =
		  LogLevel.DEFAULT
		| LogLevel.INFO
		| LogLevel.ALERT
		| LogLevel.SUCCESS
		| LogLevel.WARN
		| LogLevel.ERROR
		| LogLevel.DEBUG
		;
		
		logger.addTransport(
			new LogConsoleTransport(consoleFormatter(name, true) as any, emitLevel),
			new LogFileTransport(join(this.logDir, name + ".log"), fileFormatter(name), emitLevel)
		);
		this.set(name, logger);
		if (name.length > longest) longest = name.length;
		return logger;
	}
}

/**
 * A default log store.
 */
export default new LogStore(__dirname + "/../../logs");
