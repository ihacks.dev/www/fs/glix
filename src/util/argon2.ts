// Imports
import {
	hash as ahash,
	verify as averify,
	argon2id,
	Options	
} from "argon2";

/**
 * The Argon2 hashing options.
 */
const options: Options & { raw: false } = {
	secret: Buffer.from(process.env.ARGON2_SECRET!, "ascii"),
	hashLength: 512,
	memoryCost: 16_384,
	saltLength: 32,
	timeCost: 64,
	type: argon2id,
	version: 19,
	raw: false
};

/**
 * Hash a password using Argon2.
 * @param password the password to hash.
 */
export const hash = async (password: string) => await ahash(password, options);

/**
 * Verify that a password is equal to some previously hashed password
 * when hashed.
 * @param data The previously generated password hash to match against.
 * @param password The raw password to match with.
 */
export const verify = async (data: string, password: string) => await averify(data, password, options);
