// Imports
import { promisify } from "util";
import { resolveTxt, setServers } from "dns";

/**
 * A promisified version of resolveText function.
 * @param hostname The hostname to fetch text records from.
 */
const rtxt = promisify(resolveTxt);

// Use cloudflare DNS servers.
setServers([ "1.1.1.1", "1.0.0.1" ]);

/**
 * Fetch text records from a hostname.
 * @param hostname The hostname to fetch the text records from.
 */
export const getTxtRecords = async (hostname: string): Promise<string[]> => (await rtxt(hostname)).map(_ => _.join(""));
