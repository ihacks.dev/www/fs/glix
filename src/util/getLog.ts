// Imports
import Logs from "./logs";
module.exports = (name: string) => Logs.get(name);

