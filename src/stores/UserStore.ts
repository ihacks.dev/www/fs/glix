// Imports
import logs from "../util/log/user";
import { Document } from "mongoose";
import { Store } from "../structure/Store";
import { hash } from "../util/argon2";
import {
	User,
	UserCreate,
	userModel,
	userCreateSchema
} from "../db/entities/User";

/**
 * A place for users on the server to chill for some time.
 */
export class UserStore extends Store<User>
{
	
	/**
	 * Initiate a new user store.
	 */
	public constructor ()
	{
		super("Users", 5000, 500);
	}
	
	/**
	 * Create a new user.
	 * @param data The user data.
	 */
	public async create (data: UserCreate): Promise<User>
	{
		data = await userCreateSchema.validateAsync(data);
		logs.debug("Creating user %s", data.email);
		const doc = await userModel.create({
			email: data.email.toLowerCase().trim(),
			displayName: data.displayName.trim().toLowerCase(),
			hash: await hash(data.password)
		});
		(doc as any).beta = true; // @FIXME: When first release, remove this.
		await doc.save();
		const user = new User(this, doc as any);
		await user.load();
		this.add(user);
		logs.success("Successfully created user %s", user.email);
		return user;
	}
	
	/**
	 * Delete a user from the store and from the database.
	 * @param search A user entity or a user's ID or email.
	 */
	public async erase (search: User | string): Promise<boolean>
	{
		let user;
		if (search instanceof User) search = search._id;
		user = await this.fetch(search);
		if (user)
		{
			await user.delete();
			this.remove(user);
			return true;
		}
		return false;
	}
	
	/**
	 * Fetch a user by their email address.
	 * @param email The user's email address.
	 */
	public async fetch (email: string): Promise<User | undefined>;
	
	/**
	 * Fetch a a user by their ID.
	 * @param id The user's ID.
	 */
	public async fetch (id: string): Promise<User | undefined>;
	
	/**
	 * Search for a user by email or userID.
	 * @param search The search data.
	 */
	public async fetch (search: string): Promise<User | undefined>
	{
		search = search.toLowerCase();
		const queryProp = search.includes("@") ? "email" : "_id"
		logs.debug("Fetching user by %s with query %s from %s", queryProp, search, "cache");
		let result: User | null | Document | undefined = this.find(user => user.email === search || user._id === search);
		if (result) return result;
		logs.debug("Fetching user by %s with query %s from %s", queryProp, search, "database");
		const query = userModel.where(queryProp, search);
		result = await userModel.findOne(query);
		if (result)
		{
			const user = await (new User(this, result as any)).load();
			this.add(user);
			return user;
		}
		logs.warn("Fetching user by %s with query %s failed. Not found.", queryProp, search);
		return;
	}
}

/**
 * A default user store.
 */
export default new UserStore().start();
