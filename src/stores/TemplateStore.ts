// Imports
import { join } from "path";
import { readFile } from "fs/promises";
import {
	compile,
	AsyncTemplateFunction
} from "ejs";
import { Store } from "../structure/Store";
import { Entity } from "../structure/Cache";

/**
 * A template store for email EJS templates.
 */
export class TemplateStore extends Store<MailTemplate>
{
	
	/**
	 * The directory where email EJS templates are stored.
	 */
	public static readonly dir: string = join(__dirname, "..", "..", "templates");
	
	/** Initiate a new template store. */
	public constructor () { super("Mailer"); }
	
	/**
	 * Load and save a template to the template store.
	 * @param name The name of the template.
	 */
	public async load (name: string): Promise<MailTemplate>
	{
		const path = join(TemplateStore.dir, name + ".ejs");
		const templateString = (await readFile(path)).toString();
		const template: MailTemplate = compile(templateString, {
			async: true,
			filename: path
		}) as any;
		template._id = name;
		this.add(template);
		return template;
	}
	
	/**
	 * Get a template from the store, or load it if it isn't already
	 * loaded.
	 * @param name The name of the template.
	 */
	public async getTemplate (name: string): Promise<MailTemplate>
	{
		if (this.has(name)) return this.get(name)!;
		const template = await this.load(name);
		return template;
	}
	
}

export type MailTemplate = AsyncTemplateFunction & Entity;

/** The default template store. */
export default new TemplateStore().start();
