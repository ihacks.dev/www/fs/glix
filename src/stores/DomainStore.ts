// Imports
import logs from "../util/log/domain";
import { Document } from "mongoose";
import { Store } from "../structure/Store";
import { User } from "../db/entities/User";
import {
	Domain,
	domainModel
} from "../db/entities/Domain";
import { PendingUser } from "../structure/PendingUser";
import { getTxtRecords } from "../util/getTxtRecords";
import { verify } from "jsonwebtoken";

export class DomainStore extends Store<Domain>
{
	
	public static readonly UNVERIFIED_DOMAIN_MAX_AGE: number = 604_800_000;
	
	private i?: number;
	private a?: number;
	
	public constructor ()
	{
		super("Domains", 30_000, 1_000);
	}
	
	public start (): this
	{
		if (this.i === undefined) this.i = setInterval(() => {
			logs.debug("Purging pending users.");
			this.purgePending(432_000)
				.then(purged => purged ? logs.info("Purged pending users!") : null)
				.catch(error => logs.error("Failed purging pending users!", error.message));
		}, 3_600_000) as any;
		if (this.a === undefined) this.a = setInterval(() => {
			logs.debug("Verifying pending domains.");
			this.verifyPending()
				.then(([ v, d ]) => v > 0 || d > 0 ? logs.info("Verified %d domains, deleted %d domains.", v, d) : null)
				.catch(error => logs.error("Failed to verify pending domains!", error.message));
		}, 120_000) as any;
		return super.start();
	}
	
	public stop (): this
	{
		if (this.a !== undefined)
		{
			clearInterval(this.a as any);
			this.a = undefined;
		}
		if (this.i !== undefined)
		{
			clearInterval(this.i as any);
			this.i = undefined;
		}
		return super.stop();
	}
	
	/**
	 * Search for a domain by ID or by host.
	 * @param search 
	 */
	public async fetch (search: string, searchOwner: boolean = false): Promise<Domain | undefined>
	{
		search = search.toLowerCase();
		const queryProp = searchOwner
			? "owner"
			: search.includes(".")
			? "host"
			: "_id";
		logs.debug("Fetching domain by %s with query %s from %s", queryProp, search, "cache");
		let result: Domain | undefined | null | Document = this.find(domain => domain.host === search || domain._id === search);
		if (result) return result;
		logs.debug("Fetching domain by %s with query %s from %s", queryProp, search, "database");
		const query = domainModel.where(queryProp, search);
		result = await domainModel.findOne(query);
		if (result)
		{
			const domain = new Domain(this, result as any);
			await domain.load();
			this.add(domain);
			return domain;
		}
		logs.warn("Fetching domain by %s with query %s failed. Not found.", queryProp, search);
		return;
	}
	
	/**
	 * Fetch the pending domains.
	 */
	public async pendingDomains ()
	{
		const pending: Domain[] = [ ];
		const results = await domainModel.find({ owner: null });
		for (let result of results)
		{
			if (this.has(result._id.toString()))
			{
				pending.push(this.get(result._id.toString())!);
				continue;
			}
			const domain = new Domain(this, result as any);
			await domain.load();
			this.add(domain);
			pending.push(domain);
		}
		return pending;
	}
	
	/**
	 * Verify pending domains if any is valid.
	 */
	public async verifyPending (): Promise<[ number, number ]>
	{
		let verifiedCount = 0;
		let deletedCount = 0;
		const domains = await this.pendingDomains();
		for (let domain of domains)
		{
			if (domain.owner === null) continue;
			if (domain.pending.size < 1 && Date.now() - domain.created_at.getTime() > DomainStore.UNVERIFIED_DOMAIN_MAX_AGE)
			{
				logs.info("Deleting domain %s due to age.", domain.host);
				await domain.delete();
				deletedCount++;
				continue;
			}
			let token: string | undefined;
			for (let i = 0; i < 10 && !token; i++)
			{
				const textList = await getTxtRecords(domain.host);
				for (let t = 0; t < textList.length && !token; t++)
				{
					const txt = textList[t];
					if (txt.substring(0, 12).toUpperCase() === "GLIX-VERIFY=")
					{
						token = txt.substring(12, txt.length);
					}
				}
			}
			if (!token) continue;
			try
			{
				const result = verify(token, process.env.ARGON2_SECRET!) as any;
				const pendingUser = domain.pending.get(result.user);
				if (!pendingUser) throw new Error("Pending user not found.");
				if (!await pendingUser.verify(token)) throw new Error("Invalid token");
				logs.success("Verified domain %s by %s", domain.host, (await pendingUser.user())!.email);
				verifiedCount++;
			} catch (err)
			{
				logs.warn("Failed to verify token for domain %s", domain.host);
				logs.error(err.message);
			}
		}
		return [ verifiedCount, deletedCount ];
	}
	
	/**
	 * Fetch a pending user that is is less than a unix timestamp.
	 * @param since The number since ms.
	 */
	public async fetchPending (since: number)
	{
		return (await this.pendingDomains())
			.reduce(
				(arr, domain) => [ ...arr as any, ...domain.pending.filter(p => p.at.getTime() < since).array() as any ] as any,
				[]
			) as PendingUser[];
	}
	
	/**
	 * Purge pending users and emptied non-owned domains.
	 * @param age The age in seconds.
	 */
	public async purgePending (age: number, since: number = Date.now()): Promise<boolean>
	{
		const pending = await this.fetchPending(since - (age * 1000));
		let purged: boolean = pending.length > 0;
		for (let p of pending)
		{
			const domain = <NonNullable<Domain>> await p.domain()!;
			const user = <NonNullable<User>> await p.user()!;
			logs.info("Purging %s from %s, age %d ms", user.email, domain.host, since - p.at.getTime());
			await p.delete();
			if (domain.pending.size < 1 && !domain.owner)
			{
				logs.debug("Deleting domain %s due to purged users.", domain.host);
				await domain.delete();
				this.remove(domain);
				logs.info("Deleted domain %s!", domain.host);
			}
		}
		return purged;
	}
	
	/**
	 * Delete a user from the store and from the database.
	 * @param search A user entity or a user's ID or email.
	 */
	public async erase (search: Domain | string): Promise<boolean>
	{
		let domain;
		if (search instanceof Domain) search = search._id;
		domain = await this.fetch(search);
		if (domain)
		{
			await domain.delete();
			this.remove(domain);
			return true;
		}
		return false;
	}
	
	/**
	 * Create a new domain.
	 * @param host The hostname of the domain.
	 * @param pendingUser The pending user.
	 */
	public async create (host: string, pendingUser: User): Promise<Domain>
	{
		if (!pendingUser.verifiedAt) throw new Error("User must be verified!");
		if (!Domain.verifyDomain(host)) throw new Error("Invalid host!");
		logs.debug("Creating domain %s by pending user %s", host, pendingUser.email);
		let doc = await domainModel.create({
			host,
			pending: [
				{
					user: pendingUser._id,
					at: Date.now()
				}
			],
			links: [
				{
					host,
					dial: "glix-domain-landingpage.glitch.me",
					port: 443
				}
			]
		}) as any;
		await doc.save();
		const domain = new Domain(this, doc as any);
		await domain.load();
		this.add(domain);
		pendingUser.touch();
		await pendingUser.sendPendingDomainEmail(domain, domain.pending.array()[0]);
		logs.success("User %s successfully created domain %s, waiting for verification.", pendingUser.email, domain.host);
		return domain;
	}
	
	/**
	 * Add a pending user to a domain or create the domain if it
	 * doesn't exist.
	 */
	public async addPending (host: string, user: User): Promise<Domain>
	{
		let domain = await this.fetch(host);
		if (!domain) return await this.create(host, user);
		await domain.add(user);
		return domain;
	}
	
	/**
	 * Fetch domains owned by a user.
	 * @param user The user.
	 */
	public async by (user: User | string): Promise<Domain[]>
	{
		if (user instanceof User) user = user._id;
		user = user.toLowerCase();
		logs.debug("Fetching domain by %s with query %s from %s", "owner", user, "database");
		const query = domainModel.where("owner", user);
		let results = await domainModel.find(query);
		let arr: Domain[] = [];
		for (let result of results)
		{
			const domain = new Domain(this, result as any);
			await domain.load();
			this.add(domain);
			arr.push(domain);
		}
		return arr;
	}
	
}

export default new DomainStore().start();
